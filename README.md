
# 码蚁免费成绩管理系统

　　坦率地讲，在不久的将来，这也许会是辽东半岛上第二好用的成绩统计系统。

![表情包](https://gitee.com/dlbz/shangma/raw/master/public/examples/timg.jpg)

　　言归正传

　　教学质量是学校教学的生命线，只有能够客观分析自己教学成败得失的教师才是一个合格的老师。这是一款注重优化成绩采集方法、丰富成绩分析维度的小学成绩统计系统，力争做到符合教师工作习惯、使用方法简单、数据分析多样、分析结果科学，为教师的试卷分析、教育科研提供数据参考。

　　开发者是一名非计算机专业的小学教师，只因工作遇到了兴趣，才有了这个小项目。为了让自己统计成绩工作变得更优雅，才边学边写这个成绩统计项目。信息录入与输出简单，支持在线单条录入和表格录入、表格输出，尽量让所有信息可管理。在操作设计中，遵循 Giles Colborne 的简约至上原则，尽量做到三步便可到达要操作的位置。统计维度参考华东师范大学教育学系王孝玲教授著作的《教育统计学》第五版。前端采用X-adminV2.2，后端采用ThinkPHP V6.0.7框架开发。


　　“情怀”的代言人罗永浩说：“科技的每一次进步，给我们带来的是更好的世界，而不是完美的世界”，因此，这个项目会一直向着更好不断奔跑着！



## 主要功能：

* 设置系统信息、单位信息管理、类别管理、网页统计结果显示项目
* 学期、班级、班主任、学科管理
* 管理员、权限、角色管理
* 学生信息管理
* 考试信息设置
* 设置参加考试学科及各学科的满分、优秀、良好、及格分数线和人数比。
* 生成学生的考试号
* 生成学生试卷标签
* 生成学生成绩采集表
* 在线录入和修改成绩、表格录入成绩、扫码枪录入成绩
* 查看成绩列表、成绩图表
* 查看成绩统计结果(表格、柱形图、折线图、雷达图、箱体图、仪表图、成绩报告)
* 查看学生历次成绩(表格+拆线图)
* 教师和学生不同身份的登录
* 设置统计项目
* 根据职务、孝感组长、班主任、管理员控制数据权限


## 界面及功能展示
以下所有数据均是模拟数据，请勿对号入座。
登录界面

![登录界面](https://gitee.com/dlbz/shangma/raw/master/public/examples/denglu.png)

欢迎页面

![欢迎页面](https://gitee.com/dlbz/shangma/raw/master/public/examples/欢迎页面.png)

考试列表
![考试列表](https://gitee.com/dlbz/shangma/raw/master/public/examples/考试列表.png)

考试设置

![考试设置](https://gitee.com/dlbz/shangma/raw/master/public/examples/考试设置.png)

考试操作一

![考试操作一](https://gitee.com/dlbz/shangma/raw/master/public/examples/考试操作一.png)

考试操作二

![考试操作二](https://gitee.com/dlbz/shangma/raw/master/public/examples/考试操作二.png)

考试操作三

![考试操作三](https://gitee.com/dlbz/shangma/raw/master/public/examples/考试操作三.png)

考试操作四

![考试操作四](https://gitee.com/dlbz/shangma/raw/master/public/examples/考试操作四.png)

扫码录入成绩

![扫码录入成绩](https://gitee.com/dlbz/shangma/raw/master/public/examples/扫码录入成绩.png)

表格录入成绩

![表格录入成绩](https://gitee.com/dlbz/shangma/raw/master/public/examples/表格录入成绩.png)

学生成绩列表

![学生成绩列表](https://gitee.com/dlbz/shangma/raw/master/public/examples/学生成绩列表.png)


已录成绩

![已录成绩](https://gitee.com/dlbz/shangma/raw/master/public/examples/已录成绩.png)

系统设置

![系统设置](https://gitee.com/dlbz/shangma/raw/master/public/examples/系统设置.png)

权限分配

![权限分配](https://gitee.com/dlbz/shangma/raw/master/public/examples/20190524164451.png)

平均分

![权限分配](https://gitee.com/dlbz/shangma/raw/master/public/examples/bjavg.png)

优秀率

![权限分配](https://gitee.com/dlbz/shangma/raw/master/public/examples/bjyouxiu.png)

及格率

![权限分配](https://gitee.com/dlbz/shangma/raw/master/public/examples/bjjige.png)

标准差

![权限分配](https://gitee.com/dlbz/shangma/raw/master/public/examples/bjbiaozhuncha.png)

箱体图

![权限分配](https://gitee.com/dlbz/shangma/raw/master/public/examples/bjchayi.png)

分数段统计，可以自行设置按试卷总分将数据分割多少份。

![分数段统计](https://gitee.com/dlbz/shangma/raw/master/public/examples/分数段统计.png)


学生历次考试成绩

![学生成绩](https://gitee.com/dlbz/shangma/raw/master/public/examples/学生成绩列表.png)



学生历次考试成绩得分率与成绩在班级、学校、全部中排序的大约位置

![学生成绩](https://gitee.com/dlbz/shangma/raw/master/public/examples/学生成绩图表.png)


学生单次考试成绩

![学生成绩报告](https://gitee.com/dlbz/shangma/raw/master/public/examples/学生查看一报告.png)
总成绩位置、各学科得分和得分率

![学生成绩查询一](https://gitee.com/dlbz/shangma/raw/master/public/examples/学生查看一.png)
总成绩位置、各学科得分和得分率

![学生成绩查询二](https://gitee.com/dlbz/shangma/raw/master/public/examples/学生查询二.png)
各学科成绩与平均分雷达图、各学科成绩位置

![学生成绩查询三](https://gitee.com/dlbz/shangma/raw/master/public/examples/学生查询三.png)




## 演示地址
[http://www.dl-sm.cn](http://www.dl-sm.cn) 或 [112.126.57.79](112.126.57.79)

演示帐号

    管理员帐号   test1    密码  123456
    学生帐号   210202201210018213   密码123   此帐号是随机生成，请忽对号入座。

里面隐藏了两个很实用的功能，如果想了解隐藏功能，请下载。

## 更新内容
    升级ThinkPHP到v6.08
    升级LayUi到2.6.5
    根据功能拆分js方法,增加一些js方法
    大部分菜单或复选框改成ajax获取。
    重新规划表格和创建模板
    表格的筛选栏中增加搜索按钮
    去掉前端重复代码，尽使用layui的模块加载方式。
    增加、删除部分权限
    增加、删除部分类别
    修改统计项目默认值
    用xs-select代替原来的ajax菜单
    修改登录验证方式
    重新规划基础数据模型和控制器
    独立成绩录入相关操作到一个应用中
    修改用户在线与密码验证的中间模型
    上移考试更多操作到考试列表中
    增加创建考试向导
    增加成绩录入分工
    增加系统恢复设置(只清楚演示数据)
    合并管理员与教师表，提供phinx升级方式及使用说明。
    修复已知的表格上传教师错误、重复输入phinx命令提示的错误、登录跳转和已录成绩显示错误
    优化雷达图学科显示、成绩位置算法、部分统计表显示等 增加学生帐号显示成绩或等级设置
    增加良好分数线，优秀、良好、合格的人数比例
    增加教师和学生时的帐号、电话和身份证号的重复验证
    增加查看班级学生名单
    修复成绩报告已知错误
    增加教研组
    增加教研组长
    增加课题和荣誉统计
    增加删除学科查看与恢复
    增加删除学科"列标识"重复验证 
    增加学号

    更多更新日志（http://www.www.dl-sm.cn.cn/login/log）

## 可能会出现的错误
学生名单提示error解决方法:
1、更新代码到最到最新:git pull https://gitee.com/dlbz/shangma.git master -f
2、升级表结构：php think migrate:run
3、重新导入基础数据:php think seed:run
4、修改cj_student字段kaohao为xuehao(做前3步只为这一下，本应写一个phinx的up方法，但是感觉因为一个字段增加一个文件有点不合理……) 
    
## 升级方法
http://www.dl-sm.cn/admin/tongbu/index

## 使用手册
https://www.kancloud.cn/llblax/abcd/789222


## 交流群

<a href="https://jq.qq.com/?_wv=1027&k=DePkb6ZX" target="_blank">QQ交流群  650156356</a>

<img src="https://gitee.com/dlbz/shangma/raw/master/public/examples/42d89069b6fbebb309b2ad2678b27d0.jpg" width="300px">

## 下一步工作
* [ ] 项目的问题修复和功能完善
* [ ] 重新整理手册


## 感谢
这个项目一直在更新和完善中，在这个磕磕绊绊的成长过程中得到了许多朋友、同仁的支持和肯定，特别是给项目技术支持和打赏的朋友们，同时也感谢留下Watch、Star、Fork的朋友和ThinkPHP、xadmin等框架，不断增加的Star也是让我们坚持下去的动力源泉。
